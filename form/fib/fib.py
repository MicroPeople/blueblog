import time


def fib(n):
    if n in [1, 2]:
        return 1
    return fib(n - 1) + fib(n - 2)


satrt = time.time()
result = fib(40)
end = time.time()
print(f'斐波拉契数列第40项为:{result},耗时：{end-satrt}秒')