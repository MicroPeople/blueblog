## Usage

```
$ pip install cython
```

create fast_fib.pyx setup.py test_fast_fib.py

```
$python setup.py build_ext --inplace
```

(Windows) rename fast_fib.cp37-win_amd64.pyd to fast_fib.pyd

(Linux) rename fast_fib.cython-37-darwin.so to fast_fib.so

```
$python test_fast_fib.py
```



